<?php
/**
 * @file
 * change_management.features.inc
 */

/**
 * Implements hook_views_api().
 */
function change_management_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function change_management_node_info() {
  $items = array(
    'change_request' => array(
      'name' => t('Change Request'),
      'base' => 'node_content',
      'description' => t('Contains all data about the change request'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'client' => array(
      'name' => t('Client'),
      'base' => 'node_content',
      'description' => t('Contains all data related to the client.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Contains all data related to a project'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
