// This shows a full config file!
module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            files: "sites/all/themes/change_management/sass/**/*.scss",
            tasks: ['compass'],
        },
        compass: {
            dist: {
                options: {
                    sassDir: 'sites/all/themes/change_management/sass/',
                    cssDir: 'sites/all/themes/change_management/css/'
                    //outputStyle: 'compressed'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : 'css/*.css'
                },
                options: {
                    watchTask: true
                }
            }
        },                
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');   

    // create custom task-list
    grunt.registerTask('default', ["browserSync", "watch", "compass"]);
};
